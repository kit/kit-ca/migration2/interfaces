English version below.

Sehr geehrte Nutzerin der KIT-CA, sehr geehrter Nutzer der KIT-CA,

das von Ihnen auf den Namen
  @__DN__@
beantragte Zertifikat wurde soeben ausgestellt. Sie finden es im Anhang
dieser E-Mail. Falls Sie der Veröffentlichung des Zertifikats zugestimmt
haben, können Sie es ebenfalls herunterladen unter
  https://www.ca.kit.edu/p/getcert-g2/@__SERIAL__@

Bitte beachten Sie, dass am 15. März 2017, 14:30 Uhr, die gesamte
Zertifikatkette der KIT-CA erneuert wurde! Wenn Sie für einen bereits mit
einem alten Zertifikat der KIT-CA in Betrieb befindlichen Server ein
Folgezertifikat erhalten haben, müssen Sie daher nicht nur das
Serverzertifikat, sondern die gesamte Zertifikatkette austauschen! Die
aktuell gültige Zertifikatkette ist unter diesem URL zu finden:
  https://pki.pca.dfn.de/kit-ca/pub/cacert/chain.txt

Hinweise zum Zusammenführen von Zertifikat und geheimem Schlüssel finden
Sie in unseren Anleitungen unter
  https://www.ca.kit.edu/p/server

Bitte beachten Sie weiter die im Dokument »Informationen für
Zertifikatinhaber« aufgeführten Regelungen:
  https://info.pca.dfn.de/doc/Info_Zertifikatinhaber.pdf
  
Bei Rückfragen können Sie sich gerne an ra@kit.edu wenden.

Mit freundlichen Grüßen,
Ihr Team der KIT-CA.

Diese E-Mail wurde automatisiert versandt vom Deutschen Forschungsnetz (DFN) im
Auftrag der KIT-CA.

-----

Dear KIT-CA user,

the certificate you have requested for
  @__DN__@
has just been issued.  You can find it attached to this mail.  If you have
agreed to have it publicized, then you can also download it from
  https://www.ca.kit.edu/p/getcert-g2/@__SERIAL__@

Please note that as of March 15, 2017, 14:30 CET, the entire certificate
chain for the KIT-CA has been replaced with a new one!  If you have
received a follow-up certificate for a server that has already been
operational with an old KIT-CA certificate, then it is therefore necessary
not only to install the new server certificate, but also the entire new
certificate chain!  The current certificate chain can be found at
  https://pki.pca.dfn.de/kit-ca/pub/cacert/chain.txt

Please find notes on how to combine the certificate and the private key at
  https://www.ca.kit.edu/p/server_en

Please also note the document »Informationen für Zertifikatinhaber«:
  https://info.pca.dfn.de/doc/Info_Zertifikatinhaber.pdf

If you have any questions, please contact ra@kit.edu.

Best regards,
Your KIT-CA team.

This is an automatically-generated e-mail sent by the German Research Network
(DFN) on behalf of the KIT-CA.
-- 
KIT Certification Authority, Steinbuch Centre for Computing, KIT, Germany
ca@kit.edu | Phone: +49 721 608-8000 | https://www.ca.kit.edu
