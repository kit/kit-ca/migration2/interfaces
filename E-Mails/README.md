# Wann wird welches Template gezogen?

| **Situation**                                                         | **Template**                                                                 |
| ---                                                                   | ---                                                                          |
| Zertifikat ausgestellt (Key extern generiert)                         | [`confirm_cert_sign_pkcs10.msg`](confirm_cert_sign_pkcs10.msg)               |
| Zertifikat ausgestellt (Key im Browser generiert -> Nutzerzertifikat) | [`confirm_cert_sign.msg`](confirm_cert_sign.msg)                             |
| Zertifikat gesperrt (RA-Op)                                           | [`revokedMail_RAOperator_ra_id_0.msg`](revokedMail_RAOperator_ra_id_0.msg)   |
| Zertifikat gesperrt (nicht RA-Op)                                     | [`revokedMail.msg`](revokedMail.msg)                                         |
| Zertifikat läuft ab (RA-Op)                                           | [`expiringMail_RAOperator_ra_id_0.msg`](expiringMail_RAOperator_ra_id_0.msg) |
| Zertifikat läuft ab (nicht RA-Op)                                     | [`expiringMail.msg`](expiringMail.msg)                                       |
